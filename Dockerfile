FROM node:alpine

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install --only=production
RUN npm install -g pm2

COPY . .

ENV PORT 8080
ENV API_HOST https://jsonplaceholder.typicode.com
ENV ELASTICSEARCH_HOST elasticsearch:9200

EXPOSE 8080

CMD ["pm2-runtime", "src/server.js"]
