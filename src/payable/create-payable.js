const { Payable } = require('../models');

const createPayableFor = transaction => {
  const feesFor = {
    credit_card: 0.05,
    debit_card: 0.03
  };

  const payableSatusFor = {
    credit_card: 'waiting_funds',
    debit_card: 'paid'
  };

  const payableDateFor = {
    debit_card: () => new Date(transaction.createdAt),
    credit_card: () => {
      const payableDate = new Date(transaction.createdAt);
      payableDate.setDate(payableDate.getDate() + 30);
      return payableDate;
    }
  };

  const generatedPayable = {
    transaction_id: transaction.id,
    amount: transaction.amount,
    status: payableSatusFor[transaction.payment_method],
    payment_date: payableDateFor[transaction.payment_method](),
    fee: feesFor[transaction.payment_method] * transaction.amount,
    user: transaction.user

  };

  return generatedPayable;
};

module.exports = {
  createPayableFor
};
