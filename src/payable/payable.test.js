const request = require('supertest');
const app = require('../app');

const { Transaction, Payable } = require('./../models');

beforeAll(async () => await initializeTables());
afterAll(async () => await destroyTables());

describe('Pagaveis -> ', () => {
  describe('Obter pagaveis -> ', () => {
    beforeAll(async () => await populateDatabase());

    test('API deve estar funcionando', async () => {
      const response = await request(app)
        .get('/api/payables')
        .set('api_key', 'pk_test');
      expect(response.status).toBe(200);
    });

    describe('Em lote -> ', () => {
      describe('Paginação -> ', () => {
        test('Deve retornar \'Bad request\' para \'limit\' igual a 0', async () => {
          const response = await request(app)
            .get('/api/payables?limit=0')
            .set('api_key', 'pk_test');

          expect(response.status).toBe(400);
        });

        test('Deve retornar \'Bad request\' para \'limit\' negativo', async () => {
          const response = await request(app)
            .get('/api/payables?limit=-1')
            .set('api_key', 'pk_test');

          expect(response.status).toBe(400);
        });

        [1, 2, 3].forEach(limit =>
          test(`Deve retornar ${limit} resultado(s)`, async () => {
            const response = await request(app)
              .get(`/api/payables?limit=${limit}`)
              .set('api_key', 'pk_test');
            const data = response.body;

            expect(data.length).toBe(limit);
          })
        );
      });

      describe('Seleção de informações -> ', () => {
        test('Deve retornar somente o campo \'id\'', async () => {
          const response = await request(app)
            .get('/api/payables?fields=id')
            .set('api_key', 'pk_test');
          expect(response.status).toBe(200);

          response.body.forEach(payable => {
            expect(Object.keys(payable).length).toBe(1);
            expect(payable).toHaveProperty('id');
          });
        });

        test('Deve retornar somente os campos \'id\', \'fee\' e \'transaction_id\'', async () => {
          const response = await request(app)
            .get('/api/payables?fields=id,fee,transaction_id')
            .set('api_key', 'pk_test');
          expect(response.status).toBe(200);

          response.body.forEach(payable => {
            expect(Object.keys(payable).length).toBe(3);
            expect(payable).toHaveProperty('id');
            expect(payable).toHaveProperty('fee');
            expect(payable).toHaveProperty('transaction_id');
          });
        });

        test('Deve retornar \'Bad request\' caso seja solicitado um campo inexistente no modelo', async () => {
          const response = await request(app)
            .get('/api/payables?fields=id,xpto')
            .set('api_key', 'pk_test');
          expect(response.status).toBe(400);
        });
      });
    });

    describe('Por ID -> ', () => {
      test('Deve retornar um pagavel válido', async () => {
        const response = await request(app)
          .get('/api/payables/1')
          .set('api_key', 'pk_test');

        expect(response.status).toBe(200);
        expect(response.body.id).toBe(1);
      });

      test('Deve retornar \'404 - Not Found\' para um id inexistente', async () => {
        const response = await request(app)
          .get('/api/payables/2999')
          .set('api_key', 'pk_test');

        expect(response.status).toBe(404);
      });
    });

    describe('Por ID da Transação -> ', () => {
      test('Deve redirecionar \'/transactions/:id/payables\' para a busca parametrizada de pagaveis', async () => {
        const response = await request(app)
          .get('/api/transactions/1/payables')
          .set('api_key', 'pk_test');
        expect(response.status).toBe(302);
      });

      test('Deve retornar uma lista vazia para uma transação inexistente', async () => {
        const response = await request(app)
          .get('/api/payables?transaction_id=29998')
          .set('api_key', 'pk_test');
        expect(response.status).toBe(200);
        expect(response.body.length).toBe(0);
      });
    });

    afterAll(async () => await destroyData());
  });
});
const initializeTables = async () => {
  await Transaction.sync({ alter: true });
  await Payable.sync({ alter: true });
};

const destroyTables = async () => {
  await Transaction.drop();
  await Payable.drop();
};

const populateDatabase = async () => {
  const transactions = [
    {
      amount: 10,
      description: 'Buying ps4 game ',
      payment_method: 'debit_card',
      card_last_digits: '4444',
      card_holder_name: 'Michael Fidelis',
      card_expiration_date: '08/24',
      user: 'pk_test'
    },
    {
      amount: 20,
      description: 'Buying memory card for PS1 ',
      payment_method: 'credit_card',
      card_last_digits: '4444',
      card_holder_name: 'Michael Fidelis',
      card_expiration_date: '08/24',
      user: 'pk_test'
    },
    {
      amount: 30,
      description: 'Art of War - Sun Tzu',
      payment_method: 'credit_card',
      card_last_digits: '4444',
      card_holder_name: 'Michael Fidelis',
      card_expiration_date: '08/24',
      user: 'pk_test'
    }
  ];

  const payables = [
    {
      id: 1,
      payable_id: 1,
      status: 'paid',
      payment_date: '2019-09-25T17:44:11.540Z',
      amount: '10.00',
      fee: '0.50',
      user: 'pk_test',
      createdAt: '2019-08-25T17:44:11.584Z',
      updatedAt: '2019-08-25T17:44:11.584Z'
    },
    {
      id: 2,
      payable_id: 2,
      status: 'waiting_funds',
      payment_date: '2019-08-25T17:44:41.253Z',
      amount: '20.00',
      fee: '0.60',
      user: 'pk_test',
      createdAt: '2019-08-25T17:44:41.254Z',
      updatedAt: '2019-08-25T17:44:41.254Z'
    },
    {
      id: 3,
      payable_id: 3,
      status: 'waiting_funds',
      payment_date: '2019-08-25T18:24:01.679Z',
      amount: '30',
      fee: '0.90',
      user: 'pk_test',
      createdAt: '2019-08-25T18:24:01.680Z',
      updatedAt: '2019-08-25T18:24:01.680Z'
    }
  ];

  await Transaction.bulkCreate(transactions);
  await Payable.bulkCreate(payables);
};

const destroyData = async () => {
  await Transaction.destroy({
    where: {},
    truncate: true
  });
  await Payable.destroy({
    where: {},
    truncate: true
  });
};
