const Joi = require('@hapi/joi');
const stringArray = require('../utils/string-array');

const payableFields = [
  'id',
  'fee',
  'amount',
  'status',
  'payment_date',
  'transaction_id',
  'createdAt',
  'updatedAt'
];

const queryPayableValidation = (request, response, next) => {
  Joi.object()
    .keys({
      limit: Joi.number()
        .positive()
        .optional(),
      offset: Joi.number()
        .min(0)
        .optional(),
      fields: stringArray
        .array()
        .items(Joi.string().valid(payableFields))
        .optional()
    })
    .unknown()
    .validate(request.query, { abortEarly: false })
    .then(() => next())
    .catch(invalid =>
      response.status(400).send({ message: 'Invalid request data', errors: invalid.details.map(error => error.message) })
    );
};

module.exports = {
  queryPayableValidation
};
