const { Payable } = require('../models');

class PayableController {
  async getPayables(request, response) {
    try {
      const limit = request.query.limit || 10;
      const offset = request.query.offset || 0;

      const query = {
        limit,
        offset
      };

      query.where = {
        user: request.headers['api_key']
      };

      if (request.query.fields) {
        query.attributes = request.query.fields.split(',');
      }

      if (request.query.transaction_id) {
        query.where.transaction_id = request.query.transaction_id;
      }

      const payables = await Payable.findAll(query);

      return response.send(payables);
    } catch (error) {
      return response.status(500).send({ message: error.message, error });
    }
  }

  async getPayableByID(request, response) {
    try {
      const payable = await Payable.findByPk(request.params.payableId);

      if (!payable) {
        return response.status(404).send({ message: 'Payable not found. ' });
      }

      if (payable.user !== request.headers['api_key']) {
        return response.status(403).send({ message: 'Forbidden. ' });
      }

      return response.send(payable);
    } catch (error) {
      return response.status(500).send({ message: error.message });
    }
  }
}

module.exports = PayableController;
