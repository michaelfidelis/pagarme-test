const express = require('express');
const {
  queryPayableValidation
} = require('./payable.validation');

const routes = new express.Router();
const PayableController = require('./payable.controller');

const payableController = new PayableController();

routes.get('/', queryPayableValidation, payableController.getPayables);
routes.get('/:payableId(\\d+)', payableController.getPayableByID);

module.exports = routes;