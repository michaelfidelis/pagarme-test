const { createPayableFor } = require('./create-payable');

describe('Pagáveis ->', () => {
  describe('Processar pagáveis ->', () => {
    describe('Cartão de crédito ->', () => {
      test('Deve gerar pagavel com taxa de 5% sobre o valor da transação', () => {
        const transactions = transactionsByPaymentMethod('credit_card');

        transactions
          .map(transaction => createPayableFor(transaction))
          .forEach(payable => {
            expect(payable.fee).toBe(payable.amount * 0.05);
          });
      });

      test('Deve gerar pagavel com data D+30 sobre a data da transação', () => {
        const transactions = transactionsByPaymentMethod('credit_card');

        transactions.forEach(transaction => {
          const payable = createPayableFor(transaction);

          const expectedDate = new Date(transaction.createdAt);
          expectedDate.setDate(expectedDate.getDate() + 30);

          expect(payable.payment_date).toEqual(expectedDate);
        });
      });

      test('Deve gerar pagavel com status \'waiting_funds\'', () => {
        const transactions = transactionsByPaymentMethod('credit_card');

        transactions
          .map(transaction => createPayableFor(transaction))
          .forEach(payable => {
            expect(payable.status).toEqual('waiting_funds');
          });
      });
    });

    describe('Cartão de débito ->', () => {
      test('Deve gerar pagavel com taxa de 3% sobre o valor da transação', () => {
        const transactions = transactionsByPaymentMethod('debit_card');

        transactions
          .map(transaction => createPayableFor(transaction))
          .forEach(payable => {
            expect(payable.fee).toBe(payable.amount * 0.03);
          });
      });

      test('Deve gerar pagavel com data D+0 sobre a data da transação', () => {
        const transactions = transactionsByPaymentMethod('debit_card');

        transactions.forEach(transaction => {
          const payable = createPayableFor(transaction);

          const expectedDate = new Date(transaction.createdAt);

          expect(payable.payment_date).toEqual(expectedDate);
        });
      });

      test('Deve gerar pagavel com status \'paid\'', () => {
        const transactions = transactionsByPaymentMethod('debit_card');

        transactions
          .map(transaction => createPayableFor(transaction))
          .forEach(payable => {
            expect(payable.status).toEqual('paid');
          });
      });
    });
  });
});

const transactionsByPaymentMethod = paymentMethod =>
  getTransactions().filter(transaction => transaction.payment_method === paymentMethod);

const getTransactions = () => [
  {
    id: 1,
    amount: 10,
    description: 'Buying ps4 game ',
    payment_method: 'debit_card',
    card_last_digits: '4444',
    card_holder_name: 'Michael Fidelis',
    card_expiration_date: '08/24',
    createdAt: '2019-08-13T21:26:23.604Z',
    updatedAt: '2019-08-13T21:26:23.604Z'
  },
  {
    id: 2,
    amount: 20,
    description: 'Buying memory card for PS1 ',
    payment_method: 'credit_card',
    card_last_digits: '4444',
    card_holder_name: 'Michael Fidelis',
    card_expiration_date: '08/24',
    createdAt: '2019-08-20T21:26:23.604Z',
    updatedAt: '2019-08-20T21:26:23.604Z'
  },
  {
    id: 3,
    amount: 30,
    description: 'Art of War - Sun Tzu',
    payment_method: 'credit_card',
    card_last_digits: '4444',
    card_holder_name: 'Michael Fidelis',
    card_expiration_date: '08/24',
    createdAt: '2019-08-23T21:26:23.604Z',
    updatedAt: '2019-08-23T21:26:23.604Z'
  },
  {
    id: 4,
    amount: 27,
    description: 'Crash Bandicoot',
    payment_method: 'debit_card',
    card_last_digits: '4444',
    card_holder_name: 'Michael Fidelis',
    card_expiration_date: '08/24',
    createdAt: '2019-08-21T21:26:23.604Z',
    updatedAt: '2019-08-21T21:26:23.604Z'
  },
  {
    id: 5,
    amount: 11.99,
    description: 'Spotify Subscription',
    payment_method: 'debit_card',
    card_last_digits: '4444',
    card_holder_name: 'Michael Fidelis',
    card_expiration_date: '08/24',
    createdAt: '2019-08-23T21:26:23.604Z',
    updatedAt: '2019-08-23T21:26:23.604Z'
  },
  {
    id: 6,
    amount: 5.13,
    description: 'Uber Ride',
    payment_method: 'debit_card',
    card_last_digits: '4444',
    card_holder_name: 'Michael Fidelis',
    card_expiration_date: '08/24',
    createdAt: '2019-08-23T21:26:23.604Z',
    updatedAt: '2019-08-23T21:26:23.604Z'
  }
];
