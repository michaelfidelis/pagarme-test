require('dotenv').config();
const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const helmet = require('helmet');
const BodyParser = require('body-parser');
const logger = require('./config/logger');
const { apiKeyValidation } = require('./app.validation');

const app = express();
app.use(cors());
app.use(helmet());
app.use(BodyParser.urlencoded({ extended: true }));
app.use(BodyParser.json());
app.use(morgan('dev', { stream: logger.stream }));



app.use('/api', apiKeyValidation, require('./routes'));

/* eslint-disable-next-line no-unused-vars */
app.use((error, request, response, next) => {
  response.status(500).send({ message: error.message });
});

module.exports = app;
