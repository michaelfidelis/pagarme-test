const Joi = require('@hapi/joi');

module.exports = Joi.extend(joi => ({
  base: joi.array(),
  name: 'array',
  coerce: (value, state, options) => (value && value.split ? value.split(',') : value)
}));