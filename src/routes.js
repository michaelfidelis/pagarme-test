const express = require('express');
const routes = new express.Router();

const TransactionRoutes = require('./transaction/transaction.routes');
const PayableRoutes = require('./payable/payable.routes');
const BalanceRoutes = require('./balance/balance.routes');

routes.use('/transactions', TransactionRoutes);
routes.use('/payables', PayableRoutes);
routes.use('/balance', BalanceRoutes);

module.exports = routes;