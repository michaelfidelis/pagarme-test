const Joi = require('@hapi/joi');

const apiKeyValidation = (request, response, next) => {
  Joi.object()
    .keys({ api_key: Joi.string().required() })
    .unknown()
    .validate(request.headers, { abortEarly: false })
    .then(() => next())
    .catch(invalid =>
      response.status(400).send({
        message: 'Invalid request headers',
        errors: invalid.details.map(error => error.message)
      })
    );
};

module.exports = {
  apiKeyValidation
};
