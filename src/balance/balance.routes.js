const express = require('express');
const routes = new express.Router();
const BalanceController = require('./balance.controller');

const balanceController = new BalanceController();

routes.get('/', balanceController.getBalance);

module.exports = routes;