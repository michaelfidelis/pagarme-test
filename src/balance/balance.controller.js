const { Balance, sequelize } = require('../models');

class BalanceController {
  async getBalance(request, response) {
    try {
      let userBalance = await Balance.findOne({
        where: { user: request.headers['api_key'] }
      });

      if (!userBalance) {
        return response.send({ paid: '0', waiting_funds: '0' });
      }

      const responseBalance = { 
        paid: userBalance.paid, 
        waiting_funds: userBalance.waiting_funds 
      };

      return response.send(responseBalance);
    } catch (error) {
      return response.status(500).send({ message: error.message });
    }
  }
}

module.exports = BalanceController;
