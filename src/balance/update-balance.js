const { Balance } = require('../models');

const getAmountFor = payable => ({
  paid: payable.status === 'paid' ? (payable.amount - payable.fee) : 0,
  waiting_funds: payable.status === 'waiting_funds' ? (payable.amount - payable.fee) : 0
});

const updateBalance = async (apiKey, payable) => {
  let userBalance = await Balance.findOne({
    where: { user: apiKey }
  });

  if (!userBalance) {
    userBalance = await Balance.create({
      ...getAmountFor(payable),
      user: apiKey
    });
  } else {
    await Balance.increment(getAmountFor(payable), { where: { user: apiKey }});
  }
};

module.exports = {
  updateBalance
};
