const request = require('supertest');
const app = require('../app');

const { Transaction, Payable, Balance } = require('./../models');

beforeAll(async () => await initializeTables());
afterAll(async () => await destroyTables());

describe('Saldo -> ', () => {
  describe('Consultar saldo -> ', () => {
    test('API deve estar funcionando', async () => {
      const response = await request(app)
        .get('/api/balance')
        .set('api_key', 'pk_test');
      expect(response.status).toBe(200);
    });

    test('Deve retornar 0 para \'recebidos\' (paid) caso não haja pagáveis.', async () => {
      const response = await request(app)
        .get('/api/balance')
        .set('api_key', 'pk_test');
      const { paid } = response.body;
      expect(+paid).toBe(0);
      expect(response.status).toBe(200);
    });

    test('Deve retornar 0 para \'a receber\' (waiting_funds) caso não haja pagáveis.', async () => {
      const response = await request(app)
        .get('/api/balance')
        .set('api_key', 'pk_test');
      const { waiting_funds } = response.body;

      expect(+waiting_funds).toBe(0);
      expect(response.status).toBe(200);
    });

    describe('Valores ->', () => {
      beforeAll(async () => await populateDatabase());

      test('Deve retornar o saldo \'recebido\', considerando as taxas', async () => {
        const payables = await Payable.findAll({ where: { status: 'paid' } });

        const paidAmount = payables.reduce((total, { amount, fee }) => (total += amount - fee), 0);

        const response = await request(app)
          .get('/api/balance')
          .set('api_key', 'pk_test');
        const { paid } = response.body;

        expect(+paid).toBe(+paidAmount);
        expect(response.status).toBe(200);
      });

      test('Deve retornar o saldo \'a receber\', considerando as taxas', async () => {
        const payables = await Payable.findAll({ where: { status: 'waiting_funds' } });
        const waitingFundsAmount = payables.reduce((total, { amount, fee }) => (total += amount - fee), 0);

        const response = await request(app)
          .get('/api/balance')
          .set('api_key', 'pk_test');
        const { waiting_funds } = response.body;

        expect(waiting_funds).toBe(waitingFundsAmount);
        expect(response.status).toBe(200);
      });

      afterAll(async () => await destroyData());
    });
  });
});

const initializeTables = async () => {
  await Transaction.sync({ alter: true });
  await Payable.sync({ alter: true });
  await Balance.sync({ alter: true });
};

const destroyTables = async () => {
  await Transaction.drop();
  await Payable.drop();
  await Balance.drop();
};

const populateDatabase = async () => {
  const transactions = [
    {
      amount: 10,
      description: 'Buying ps4 game ',
      payment_method: 'debit_card',
      card_number: '1111222233334444',
      card_holder_name: 'Michael Fidelis',
      card_expiration_date: '08/24',
      card_cvv: '733'
    },
    {
      amount: 12,
      description: 'Buying memory card for PS1 ',
      payment_method: 'credit_card',
      card_number: '1111222233334444',
      card_holder_name: 'Michael Fidelis',
      card_expiration_date: '08/24',
      card_cvv: '733'
    },
    {
      amount: 12,
      description: 'Art of War - Sun Tzu',
      payment_method: 'credit_card',
      card_number: '1111222233334444',
      card_holder_name: 'Michael Fidelis',
      card_expiration_date: '08/24',
      card_cvv: '733'
    },
    {
      amount: 10,
      description: 'Little Big Plannet',
      payment_method: 'debit_card',
      card_number: '1111222233334444',
      card_holder_name: 'Michael Fidelis',
      card_expiration_date: '08/24',
      card_cvv: '733'
    },
    {
      amount: 27,
      description: 'Ultra Wide LCD Monitor',
      payment_method: 'debit_card',
      card_number: '1111222233334444',
      card_holder_name: 'Michael Fidelis',
      card_expiration_date: '08/24',
      card_cvv: '733'
    }
  ];

  await request(app)
    .post('/api/transactions')
    .set('api_key', 'pk_test')
    .send(transactions[0]);

  await request(app)
    .post('/api/transactions')
    .set('api_key', 'pk_test')
    .send(transactions[1]);

  await request(app)
    .post('/api/transactions')
    .set('api_key', 'pk_test')
    .send(transactions[2]);

  await request(app)
    .post('/api/transactions')
    .set('api_key', 'pk_test')
    .send(transactions[3]);

  await request(app)
    .post('/api/transactions')
    .set('api_key', 'pk_test')
    .send(transactions[4]);
};

const destroyData = async () => {
  await Transaction.destroy({
    where: {},
    truncate: true
  });
  await Payable.destroy({
    where: {},
    truncate: true
  });
  await Balance.destroy({
    where: {},
    truncate: true
  });
};
