'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('balance', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      paid: {
        allowNull: false,
        type: Sequelize.DECIMAL(10, 2, 'int'),
        defaultValue: '0'
      },
      waiting_funds: {
        allowNull: false,
        type: Sequelize.DECIMAL(10, 2, 'int'),
        defaultValue: '0'
      },
      user: {
        allowNull: false,
        type: Sequelize.STRING,
        unique: true
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('balance');
  }
};
