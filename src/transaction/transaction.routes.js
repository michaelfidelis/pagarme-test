const express = require('express');
const routes = new express.Router();
const TransactionController = require('./transaction.controller');
const {
  processTransactionValidation,
  queryTransactionValidation
} = require('./transaction.validation');

const transactionController = new TransactionController();

routes.post('/', processTransactionValidation, transactionController.processTransaction);
routes.get('/', queryTransactionValidation, transactionController.getTransactions);
routes.get('/:transactionId(\\d+)', transactionController.getTransactionByID);
routes.use('/:transactionId(\\d+)/payables', transactionController.getPayablesForTransaction);

module.exports = routes;
