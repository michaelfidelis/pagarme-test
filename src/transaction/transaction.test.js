const request = require('supertest');
const app = require('../app');

const { Transaction, Payable, Balance } = require('./../models');

beforeAll(async () => await initializeTables());
afterAll(async () => await destroyTables());

describe('Transações -> ', () => {
  describe('Obter transações -> ', () => {
    beforeAll(async () => await populateDatabase());

    test('API deve estar funcionando', async () => {
      const response = await request(app)
        .get('/api/transactions')
        .set('api_key', 'pk_test');
      expect(response.status).toBe(200);
    });

    describe('Em lote -> ', () => {
      describe('Paginação -> ', () => {
        test('Deve retornar \'Bad request\' para \'limit\' igual a 0', async () => {
          const response = await request(app)
            .get('/api/transactions?limit=0')
            .set('api_key', 'pk_test');

          expect(response.status).toBe(400);
        });

        test('Deve retornar \'Bad request\' para \'limit\' negativo', async () => {
          const response = await request(app)
            .get('/api/transactions?limit=-1')
            .set('api_key', 'pk_test');

          expect(response.status).toBe(400);
        });

        [1, 2, 3].forEach(limit =>
          test(`Deve retornar ${limit} resultado(s)`, async () => {
            const response = await request(app)
              .get(`/api/transactions?limit=${limit}`)
              .set('api_key', 'pk_test');
            const data = response.body;

            expect(data.length).toBe(limit);
          })
        );
      });

      describe('Seleção de informações -> ', () => {
        test('Deve retornar somente o campo \'id\'', async () => {
          const response = await request(app)
            .get('/api/transactions?fields=id')
            .set('api_key', 'pk_test');
          expect(response.status).toBe(200);

          response.body.forEach(transaction => {
            const transactionKeys = Object.keys(transaction);

            expect(transactionKeys.length).toBe(1);
            expect(transactionKeys.includes('id')).toBeTruthy();
          });
        });

        test('Deve retornar somente os campos \'id\', \'amount\' e \'description\'', async () => {
          const response = await request(app)
            .get('/api/transactions?fields=id,amount,description')
            .set('api_key', 'pk_test');
          expect(response.status).toBe(200);

          response.body.forEach(transaction => {
            const transactionKeys = Object.keys(transaction);

            expect(transactionKeys.length).toBe(3);
            expect(transactionKeys.includes('id')).toBeTruthy();
            expect(transactionKeys.includes('amount')).toBeTruthy();
            expect(transactionKeys.includes('description')).toBeTruthy();
          });
        });

        test('Deve retornar \'Bad request\' caso seja solicitado um campo inexistente no modelo', async () => {
          const response = await request(app)
            .get('/api/transactions?fields=id,xpto')
            .set('api_key', 'pk_test');
          expect(response.status).toBe(400);
        });
      });
    });

    describe('Por ID -> ', () => {
      test('Deve retornar uma transação válida', async () => {
        const response = await request(app)
          .get('/api/transactions/1')
          .set('api_key', 'pk_test');

        expect(response.status).toBe(200);
        expect(response.body.id).toBe(1);
      });

      test('Deve retornar \'404 - Not Found\' para um id inexistente', async () => {
        const response = await request(app)
          .get('/api/transactions/0')
          .set('api_key', 'pk_test');

        expect(response.status).toBe(404);
      });
    });

    afterAll(async () => await destroyData());
  });

  describe('Processar transações -> ', () => {
    describe('Validação -> ', () => {
      describe('Deve retornar \'400 - Bad request\' -> ', () => {
        test('Quando o número do cartão não for válido ', async () => {
          const transaction = {
            amount: 27,
            description: 'Buying ps4 game ',
            payment_method: 'debit_card',
            card_number: '0000',
            card_holder_name: 'Michael Fidelis',
            card_expiration_date: '08/24',
            card_cvv: '733'
          };

          const response = await request(app)
            .post('/api/transactions')
            .set('api_key', 'pk_test')
            .send(transaction);

          const responseBody = response.body;
          const hasCardNumberMessage = responseBody.errors.some(error => error.includes('card_number'));

          expect(hasCardNumberMessage).toBeTruthy();
          expect(response.status).toBe(400);
        });

        test('Quando o valor da compra não for válido ', async () => {
          const transaction = {
            amount: '',
            description: 'Buying ps4 game ',
            payment_method: 'debit_card',
            card_number: '1111222244443333',
            card_holder_name: 'Michael Fidelis',
            card_expiration_date: '08/24',
            card_cvv: '733'
          };

          const response = await request(app)
            .post('/api/transactions')
            .set('api_key', 'pk_test')
            .send(transaction);

          const responseBody = response.body;
          const hasAmountMessage = responseBody.errors.some(error => error.includes('amount'));

          expect(hasAmountMessage).toBeTruthy();
          expect(response.status).toBe(400);
        });

        test('Quando o nome no cartão não for válido ', async () => {
          const transaction = {
            amount: 27,
            description: 'Buying ps4 game ',
            payment_method: 'debit_card',
            card_number: '1111222233334444',
            card_holder_name: '',
            card_expiration_date: '08/24',
            card_cvv: '733'
          };

          const response = await request(app)
            .post('/api/transactions')
            .set('api_key', 'pk_test')
            .send(transaction);

          const responseBody = response.body;
          const hasCardHolderNameMessage = responseBody.errors.some(error => error.includes('card_holder_name'));

          expect(hasCardHolderNameMessage).toBeTruthy();
          expect(response.status).toBe(400);
        });

        test('Quando a descrição da compra não for válida ', async () => {
          const transaction = {
            amount: 27,
            description: '',
            payment_method: 'debit_card',
            card_number: '1111222244443333',
            card_holder_name: 'Michael Fidelis',
            card_expiration_date: '08/24',
            card_cvv: '733'
          };

          const response = await request(app)
            .post('/api/transactions')
            .set('api_key', 'pk_test')
            .send(transaction);

          const responseBody = response.body;
          const hasDescriptionMessage = responseBody.errors.some(error => error.includes('description'));

          expect(hasDescriptionMessage).toBeTruthy();
          expect(response.status).toBe(400);
        });

        test('Quando o método de pagamento não for válido ', async () => {
          const transaction = {
            amount: 2.99,
            description: 'Art of War - Sun Tzu',
            payment_method: 'elixir_coin',
            card_number: '1111222233334444',
            card_holder_name: 'Michael Fidelis',
            card_expiration_date: '08/24',
            card_cvv: '733'
          };

          const response = await request(app)
            .post('/api/transactions')
            .set('api_key', 'pk_test')
            .send(transaction);

          const responseBody = response.body;
          const hasPaymentMethodMessage = responseBody.errors.some(error => error.includes('payment_method'));

          expect(hasPaymentMethodMessage).toBeTruthy();
          expect(response.status).toBe(400);
        });

        test('Quando data de expiração do cartão não for válida ', async () => {
          const transaction = {
            amount: 2.99,
            description: 'Art of War - Sun Tzu',
            payment_method: 'credit_card',
            card_number: '1111222233334444',
            card_holder_name: 'Michael Fidelis',
            card_expiration_date: '20/24',
            card_cvv: '733'
          };

          const response = await request(app)
            .post('/api/transactions')
            .set('api_key', 'pk_test')
            .send(transaction);

          const responseBody = response.body;
          const hasExpirationDateMessage = responseBody.errors.some(error => error.includes('card_expiration_date'));

          expect(hasExpirationDateMessage).toBeTruthy();
          expect(response.status).toBe(400);
        });

        test('Quando o CVV não for válido', async () => {
          const transaction = {
            amount: 2.99,
            description: 'Art of War - Sun Tzu',
            payment_method: 'credit_card',
            card_number: '1111222233334444',
            card_holder_name: 'Michael Fidelis',
            card_expiration_date: '08/24',
            card_cvv: '0000'
          };

          const response = await request(app)
            .post('/api/transactions')
            .set('api_key', 'pk_test')
            .send(transaction);

          const responseBody = response.body;
          const hasCVVMessage = responseBody.errors.some(error => error.includes('card_cvv'));

          expect(hasCVVMessage).toBeTruthy();
          expect(response.status).toBe(400);
        });
      });
    });

    test('Deve processar uma transação com sucesso', async () => {
      const transaction = {
        amount: 27,
        description: 'Buying ps4 game ',
        payment_method: 'debit_card',
        card_number: '1111222233334444',
        card_holder_name: 'Michael Fidelis',
        card_expiration_date: '08/24',
        card_cvv: '733'
      };

      const response = await request(app)
        .post('/api/transactions')
        .set('api_key', 'pk_test')
        .send(transaction);

      expect(response.status).toBe(200);
    });

    test('Deve guardar somente os 4 ultimos dígitos do cartão', async () => {
      const transaction = {
        amount: 27,
        description: 'Buying ps4 game ',
        payment_method: 'debit_card',
        card_number: '1111222233334444',
        card_holder_name: 'Michael Fidelis',
        card_expiration_date: '08/22',
        card_cvv: '741'
      };

      const response = await request(app)
        .post('/api/transactions')
        .set('api_key', 'pk_test')
        .send(transaction);

      const transactionResult = response.body;

      expect(response.status).toBe(200);
      expect(transactionResult.card_last_digits).toBe('4444');
    });

    test('Não deve retornar o CVV', async () => {
      const transaction = {
        amount: 27,
        description: 'Buying ps4 game ',
        payment_method: 'debit_card',
        card_number: '1111222233334444',
        card_holder_name: 'Michael Fidelis',
        card_expiration_date: '08/22',
        card_cvv: '741'
      };

      const response = await request(app)
        .post('/api/transactions')
        .set('api_key', 'pk_test')
        .send(transaction);

      const transactionResult = response.body;

      expect(response.status).toBe(200);
      expect(response.body).toBeTruthy();
      expect(transactionResult.cvv).toBeUndefined();
    });

    afterEach(async () => await destroyData());
  });
});

const initializeTables = async () => {
  await Transaction.sync({ alter: true });
  await Payable.sync({ alter: true });
  await Balance.sync({ alter: true });
};

const destroyTables = async () => {
  await Transaction.drop();
  await Payable.drop();
  await Balance.drop();
};

const populateDatabase = async () => {
  const transactions = [
    {
      amount: 10,
      description: 'Buying ps4 game ',
      payment_method: 'debit_card',
      card_last_digits: '4444',
      card_holder_name: 'Michael Fidelis',
      card_expiration_date: '08/24',
      user: 'pk_test'
    },
    {
      amount: 20,
      description: 'Buying memory card for PS1 ',
      payment_method: 'credit_card',
      card_last_digits: '4444',
      card_holder_name: 'Michael Fidelis',
      card_expiration_date: '08/24',
      user: 'pk_test'
    },
    {
      amount: 30,
      description: 'Art of War - Sun Tzu',
      payment_method: 'credit_card',
      card_last_digits: '4444',
      card_holder_name: 'Michael Fidelis',
      card_expiration_date: '08/24',
      user: 'pk_test'
    }
  ];

  const payables = [
    {
      id: 1,
      transaction_id: 1,
      status: 'paid',
      payment_date: '2019-09-25T17:44:11.540Z',
      amount: '10.00',
      fee: '0.50',
      createdAt: '2019-08-25T17:44:11.584Z',
      updatedAt: '2019-08-25T17:44:11.584Z'
    },
    {
      id: 2,
      transaction_id: 2,
      status: 'waiting_funds',
      payment_date: '2019-08-25T17:44:41.253Z',
      amount: '20.00',
      fee: '0.60',
      createdAt: '2019-08-25T17:44:41.254Z',
      updatedAt: '2019-08-25T17:44:41.254Z'
    },
    {
      id: 3,
      transaction_id: 3,
      status: 'waiting_funds',
      payment_date: '2019-08-25T18:24:01.679Z',
      amount: '30',
      fee: '0.90',
      createdAt: '2019-08-25T18:24:01.680Z',
      updatedAt: '2019-08-25T18:24:01.680Z'
    }
  ];

  await Transaction.bulkCreate(transactions);
  await Payable.bulkCreate(payables);
};

const destroyData = async () => {
  await Transaction.destroy({
    where: {},
    truncate: true
  });
  await Payable.destroy({
    where: {},
    truncate: true
  });
  await Balance.destroy({
    where: {},
    truncate: true
  });
  
};
