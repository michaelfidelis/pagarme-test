const { Transaction, Payable } = require('../models');
const { createPayableFor } = require('../payable/create-payable');
const { updateBalance } = require('./../balance/update-balance');
class TransactionController {
  async processTransaction(request, response) {
    try {
      const transactionData = request.body;

      const transaction = await Transaction.create({
        amount: transactionData.amount,
        description: transactionData.description,
        payment_method: transactionData.payment_method,
        card_last_digits: transactionData.card_number.substr(12),
        card_holder_name: transactionData.card_holder_name,
        card_expiration_date: transactionData.card_expiration_date,
        user: request.headers['api_key']
      });

      const generatedPayable = createPayableFor(transaction);
      await Payable.create(generatedPayable);

      await updateBalance(transaction.user, generatedPayable);

      return response.send(transaction);
    } catch (error) {
      return response.status(500).send({ message: error.message, errpr: error.stack });
    }
  }

  async getTransactions(request, response) {
    try {
      const limit = request.query.limit || 10;
      const offset = request.query.offset || 0;

      const query = {
        limit,
        offset,
        where: {
          user: request.headers['api_key']
        }
      };

      if (request.query.fields) {
        query.attributes = request.query.fields.split(',');
      }

      const transactions = await Transaction.findAll(query);

      return response.send(transactions);
    } catch (error) {
      return response.status(500).send({ message: error.message });
    }
  }

  async getTransactionByID(request, response) {
    try {
      const transaction = await Transaction.findByPk(request.params.transactionId);

      if (!transaction) {
        return response.status(404).send({ message: 'Transaction not found. ' });
      }

      if (transaction.user !== request.headers['api_key']) {
        return response.status(403).send({ message: 'Forbidden. ' });
      }

      return response.send(transaction);
    } catch (error) {
      return response.status(500).send({ message: error.message });
    }
  }

  async getPayablesForTransaction(request, response) {
    response.header('api_key', request.headers['api_key']);
    return response.redirect(`../../payables?transaction_id=${request.params.transactionId}`);
  }
}

module.exports = TransactionController;
