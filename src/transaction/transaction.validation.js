const Joi = require('@hapi/joi');
const stringArray = require('../utils/string-array');

const transactionFields = [
  'id',
  'amount',
  'description',
  'payment_method',
  'card_last_digits',
  'card_holder_name',
  'card_expiration_date',
  'createdAt',
  'updatedAt'
];

const processTransactionValidation = (request, response, next) => {
  Joi.object()
    .keys({
      amount: Joi.number()
        .positive()
        .precision(2)
        .required(),
      description: Joi.string()
        .min(3)
        .max(30)
        .required(),
      payment_method: Joi.string()
        .valid(['debit_card', 'credit_card'])
        .required(),
      card_number: Joi.string()
        .creditCard()
        .required(),
      card_holder_name: Joi.string().required(),
      card_expiration_date: Joi.string()
        .regex(/^(((0)[0-9])|((1)[0-2]))(\/)\d{2}$/)
        .required(),
      card_cvv: Joi.string()
        .regex(/^\d{3}$/)
        .required()
    })
    .validate(request.body, { abortEarly: false })
    .then(() => next())
    .catch(invalid =>
      response.status(400).send({ message: 'Invalid request data', errors: invalid.details.map(error => error.message) })
    );
};

const queryTransactionValidation = (request, response, next) => {
  Joi.object()
    .keys({
      limit: Joi.number()
        .positive()
        .optional(),
      offset: Joi.number()
        .min(0)
        .optional(),
      fields: stringArray
        .array()
        .items(Joi.string().valid(transactionFields))
        .optional()
    })
    .validate(request.query, { abortEarly: false })
    .then(() => next())
    .catch(invalid =>
      response.status(400).send({ message: 'Invalid request data', errors: invalid.details.map(error => error.message) })
    );
};

module.exports = {
  processTransactionValidation,
  queryTransactionValidation
};
