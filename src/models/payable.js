'use strict';
module.exports = (sequelize, DataTypes) => {
  const Payable = sequelize.define('Payable', {
    transaction_id: DataTypes.INTEGER,
    status: DataTypes.STRING,
    payment_date: DataTypes.DATE,
    amount: DataTypes.DECIMAL(10, 2), 
    fee: DataTypes.DECIMAL(10, 2),
    user: DataTypes.STRING
  }, {tableName: 'payables'});
  return Payable;
};