'use strict';
module.exports = (sequelize, DataTypes) => {
  const Transaction = sequelize.define('Transaction', {
    amount: DataTypes.DECIMAL(10,2),
    description: DataTypes.STRING,
    payment_method: DataTypes.ENUM(['debit_card', 'credit_card']),
    card_last_digits: DataTypes.STRING,
    card_holder_name: DataTypes.STRING,
    card_expiration_date: DataTypes.STRING,
    user: DataTypes.STRING
  }, {tableName: 'transactions'});
  return Transaction;
};