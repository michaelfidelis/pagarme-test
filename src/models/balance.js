'use strict';
module.exports = (sequelize, DataTypes) => {
  const Balance = sequelize.define(
    'Balance',
    {
      paid: DataTypes.DECIMAL(10, 2),
      waiting_funds: DataTypes.DECIMAL(10, 2),
      user: DataTypes.STRING
    },
    { tableName: 'balance' }
  );
  return Balance;
};
