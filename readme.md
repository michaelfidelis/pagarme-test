# Desafio PagarMe
> Desafio para a vaga de Software Engineer 

## Instalação
 
Se ja tiver o Postgres instalado configure as variáveis no arquivo `.env` usando `.env.example` como guia,
senão pode usar o docker-compose para iniciar o Postgres. 

Para isso, copie as informações do arquivo `.env.default-docker-compose` para o arquivo `.env` e execute `docker-compose up -d` na pasta do projeto.

### Instale as dependencias e execute os comandos necessários
```sh
//Usando Yarn
yarn install
yarn migrate

// Usando NPM
npm install
npm run migrate
```
### Testes

o SQLite é utilizado no lugar do Postgres durante os testes

```sh
//Usando Yarn
yarn test

// Usando NPM
npm run test
```

### Inicie o projeto
```sh
// Usando YARN
yarn migrate
yarn dev

// Usando NPM
npm run migrate
npm run dev
```

### Use a coleção do Postman
Na raíz do projeto há o arquivo `api.postman_collection.json` com exemplos das chamadas desta API

É necessário configurar as variáveis **{{host}}** e **{{api_key}}**

A variável **{{api_key}}** é utilizada somente para a identificação do cliente e suas transações. 

Qualquer valor no formato de texto pode ser utilizado.

Tambem disponibilizado em https://documenter.getpostman.com/view/73007/SVfQR9Ci

## Resumo da API
Verbo | Endpoint | Descrição
------------ | ------------ | -------------
POST | /api/transactions | Recebe os dados para realizar o processamento da transação 
GET | /api/transactions |  Retorna uma lista de transações criadas
GET | /api/transactions/:transactionId |  Retornar a transação 
GET | /api/transactions/:transactionId/payables |  Retorna os _payables_ de uma transação 
- | - | -
GET | /api/payables |  Retorna todos os _payables_  
GET | /api/payables/:payableId |  Retorna o _payable_ por Id
- | - | -
GET | /api/balance |  Retorna o saldo de _payables_ agrupados por _status_  

## Estrutura dos objetos

### Transaction  
```json
{
    "id": "Integer",
    "amount": "Decimal(10,2)",
    "description": "String",
    "payment_method": "Enum[debit_card | credit_card]",
    "card_number": "String(//d+)",
    "card_holder_name": "String",
    "card_expiration_date": "String(/^\d{2}\/\d{2}$/)",
    "card_cvv": "String(/[0-9]{3}/)"
}
```

### Payable
```json
{
    "id": "Integer",
    "transaction_id": "Integer",
    "status": "Enum[paid|waiting_funds]",
    "payment_date": "Date (ISO)",
    "amount": "Decimal(10,2)",
    "fee": "Decimal(10,2)",
}
```

### Balance 
```json
{
    "paid": "Decimal(10,2)",
    "waiting_funds": "Decimal(10,2)"
}
```

